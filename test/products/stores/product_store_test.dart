import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:value_notifier/products/services/product_service.dart';
import 'package:value_notifier/products/states/product_state.dart';
import 'package:value_notifier/products/stores/product_store.dart';

class ProducServiceMock extends Mock implements ProductService {}

void main() {
  final service = ProducServiceMock();
  final store = ProductStore(service);

  test('should change state to success', () async {
    when(() => service.fetchProduct()).thenAnswer((_) async => []);
    await store.fetchProducts();
    expect(store.value, isA<SuccessProducState>());
  });

  test('should change state to error', () async {
    when(() => service.fetchProduct()).thenThrow(Exception('Error'));
    await store.fetchProducts();
    expect(store.value, isA<ErrorProducState>());
  });
}
