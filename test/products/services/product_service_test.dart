import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:uno/uno.dart';
import 'package:value_notifier/products/services/product_service.dart';

import '../../mocks/products_mock.dart';

class UnoMock extends Mock implements Uno {}

class RequestMock extends Mock implements Request {}

void main() {
  final unoMock = UnoMock();
  final service = ProductService(unoMock);

  test('should list all products', () async {
    when(() => unoMock.get(any())).thenAnswer(
      (_) async => Response(
        headers: {},
        request: RequestMock(),
        status: 200,
        data: productMock,
      ),
    );

    final products = await service.fetchProduct();
    expect(products.first.title, 'Flutter 2');
  });
}
