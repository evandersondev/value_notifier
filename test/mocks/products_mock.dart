import 'dart:convert';

final productMock = jsonDecode(r'''
[
  {
    "id": "0",
    "title": "Flutter 2"
  },
  {
    "id": "1",
    "title": "React Native"
  },
  {
    "title": "Ionic",
    "id": "2"
  }
]
''');
