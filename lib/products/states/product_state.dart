import 'package:value_notifier/products/models/product_model.dart';

abstract class ProductState {}

// Initial, Success, Error, Loading
class InitialProducState extends ProductState {}

class SuccessProducState extends ProductState {
  SuccessProducState(this.products);

  final List<ProductModel> products;
}

class ErrorProducState extends ProductState {
  final String message;

  ErrorProducState(this.message);
}

class LoadingProducState extends ProductState {}
