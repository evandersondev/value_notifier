import 'package:flutter/material.dart';
import 'package:value_notifier/products/services/product_service.dart';
import 'package:value_notifier/products/states/product_state.dart';

class ProductStore extends ValueNotifier<ProductState> {
  ProductStore(this.server) : super(InitialProducState());

  final ProductService server;

  Future<void> fetchProducts() async {
    value = LoadingProducState();
    Future.delayed(const Duration(seconds: 1));

    try {
      final products = await server.fetchProduct();
      value = SuccessProducState(products);
    } catch (e) {
      value = ErrorProducState(e.toString());
    }
  }
}
