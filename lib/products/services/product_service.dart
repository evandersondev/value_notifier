import 'package:uno/uno.dart';
import 'package:value_notifier/products/models/product_model.dart';

class ProductService {
  ProductService(this.uno);

  final Uno uno;

  Future<List<ProductModel>> fetchProduct() async {
    final response = await uno.get('http://10.0.2.2:3031/products');
    final list = response.data as List;
    final products = list.map((e) => ProductModel.fromMap(e)).toList();
    return products;
  }
}
