<h1 align="center">VALUE NOTIFIER</h1>

<h4 align="center">Projeto de estudo sobre ValueNotifier do Flutter</h4>

<p align="center">
  <a href="https://opensource.org/licenses/MIT">
    <img src="https://img.shields.io/badge/License-MIT-blue.svg" alt="License MIT">
  </a>
</p>

<hr />

## Features

[//]: # 'Add the features of your project here:'

Features used in the Project.
- Flutter v2.10.3
- TDD
- SOLID
- Dartion (generate local server)

Libs.
- Uno v1.0.1+3
- Mocktail v0.3.0
- Provider v6.0.2

### - LICENSE

This project is licensed under the MIT License - see the <a href="https://opensource.org/licenses/MIT" target="_blank">LICENSE</a> page for details.